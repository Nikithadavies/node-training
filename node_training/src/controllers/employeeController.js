const uuid = require('uuid').v4;
const employees = [];

exports.getEmployees = (req, res) => {
    res.send(employees);
};

exports.createEmployee = (req, res) => {
  console.log(JSON.stringify(req.body));
  const employee = { ...req.body };
  employee.id = uuid();
  employees.push(employee);
  res.json(employee);
};
exports.getemployeeId = (req,res) => {
  const id=req.params.id;
  const employee=[];
  var i;
  for(i=0;i<employees.length;i++){
    if(employees[i].id == id )
      employee.push(employees[i]);
   }
        console.log(employee);
        res.json(employee);
 };
exports.deleteEmployee = (req,res) => {
  console.log("delete: req.body: " + JSON.stringify(req.body));
  const id=req.body.id;
  var i;
  for(i=0;i<employees.length;i++){
    if(employees[i].id == id )
      employees.splice(i,1);
       }
        console.log(employees);
        res.json(employees);

};
exports.updateEmployee = (req,res) => {
  console.log("Update: req.body: " + JSON.stringify(req.body));
  const id=req.body.id;
  const employee = { ...req.body };
  var i;
  for(i=0;i<employees.length;i++){
    if(employees[i].id == id )
      employees.splice(i,1);
       }
        employees.push(employee);
        console.log(employees);
        res.json(employees);
};

 exports.getbyDepartment = (req,res) =>{
   const dept=req.params.department;
   const employee=[];
   var i;
  for(i=0;i<employees.length;i++){
    if(employees[i].department==dept)
      employee.push(employees[i]);
   }
        console.log(employee);
        res.json(employee);
 }

